var webpack = require('webpack'),
    path = require('path');
 
    distPath = path.join(__dirname, '/dist');

var ExtractTextPlugin = require('extract-text-webpack-plugin');
 
module.exports = {
    cache: true,
    entry: {
        styles: './scss/main.scss',
    },
    output: {
        path: distPath,
        filename: '[name].css',
        library: '[name]'
    },
    module: {
        rules: [{
            test: /\.scss$/,
            loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
        }]
    },
    plugins: [
        new ExtractTextPlugin('styles.css', {
            allChunks: true
        })
    ],
};
